'use strict';

function userConfig(prefix, description) {
    return {
        userId: {
            doc: `userId for a ${description}`,
            format: String,
            default: null
        },
        fullName: {
            doc: `fullName for a ${description}`,
            format: String,
            default: null
        },
        email: {
            doc: `email for a ${description}`,
            format: 'email',
            default: null
        },
        password: {
            doc: `password for a ${description}`,
            format: String,
            default: null
        }
    };
}

module.exports = {

    xpsearchUserSearcher: {
        baseUrl: {
            doc: 'xpsearch-user-searcher we are using to run the tests',
            format: 'url',
            env: 'SERVICE_URL',
            default: null
        },
        asap: {
            keyDataUri: {
                doc: 'Key URI',
                format: 'String',
                default: process.env['ASAP_PRIVATE_KEY']
            },
            issuer: {
                doc: 'Key issuer',
                format: 'String',
                default: process.env['ASAP_ISSUER']
            },
            audience: {
                doc: 'Key audience',
                format: 'String',
                default: 'xpsearch-user-searcher'
            }
        }
    },
    atlassianAccount: {
        token: {
            doc: 'aid-account bearer token',
            format: '*',
            default: null
        },
        deleteToken: {
            doc: 'aid-acccount bearer token',
            format: '*',
            default: null
        }
    },
    cookieName: {
        doc: 'Name of cookie where session token is stored',
        format: 'String',
        default: 'cloud.session.token'
    },
    management: {
        baseUrl: {
            doc: 'aid-management url we are using to run the tests',
            format: 'url',
            default: null
        }
    },
    signup: {
        baseUrl: {
            doc: 'Aid-signup we are using to run the tests',
            format: 'url',
            default: null
        }
    },
    userAccounts: {
        basicUser: userConfig('BASIC', 'basic user'),
        cloudId: {
            doc: 'cloud id of the site used to test',
            format: String,
            default: null
        }
    }
};
