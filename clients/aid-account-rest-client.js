'use strict';

const config = require('../../configuration');
const httplease = require('httplease');

const baseUrl = config.account.baseUrl;
const uuid = require('uuid/v4');
const crypto = require('crypto');
const aidToken = config.atlassianAccount.token;
const aidMngtToken = config.atlassianAccount.deleteToken;

class AidAccountRestClient {

    constructor({basePath = ''} = {}) {
        this.httpClient = httplease.builder()
            .withBaseUrl(baseUrl + basePath)
            .withTimeout(30000)
            .withBufferJsonResponseHandler();

        this.httpAuthClient = this.httpClient
            .withHeaders({
                'Authorization' : `Bearer ${aidToken}`
            });

        this.httpAuthMgmtClient = this.httpClient
            .withHeaders({
                'Authorization' : `Bearer ${aidMngtToken}`
            });
    }

    buildUser({email, displayName, password, validatedEmails}) {
        const submitDisplayName = displayName || email;
        const submitPassword = password || 'password';
        const submitValidatedEmails = validatedEmails || [email];
        return {
            schemas: ['urn:scim:schemas:core:2.0', 'urn:scim:schemas:extension:atlassian:1.0'],
            displayName: submitDisplayName,
            userName: email,
            password: submitPassword,
            emails: [{
                value: email,
                primary: true
            }],
            'urn:scim:schemas:extension:atlassian:1.0': {
                validatedEmails: submitValidatedEmails
            }
        };
    }

    async getAuthToken({email, password}) {

        const path = '/verifyCredentials';
        const body = {
            username: email,
            password: password
        };

        console.log(`[AidAccountRestClient] POST ${this.httpClient.config.baseUrl}${path}`);
        console.log(`[AidAccountRestClient] >> ${JSON.stringify(body)}`);
        return await this.httpClient
            .withPath(path)
            .withMethodPost()
            .withJsonBody(body)
            .withExpectStatus([200])
            .send()
            .then((response) => {
                return response.body.token;
            });
    }

    async searchUser({email}) {
        const path = '/Users/.search';
        console.log(`[AidAccountRestClient] POST ${this.httpClient.config.baseUrl}${path}`);
        return await this.httpAuthClient
            .withMethodPost()
            .withPath(path)
            .withJsonBody({
                schemas: ['urn:scim:schemas:core:2.0:SearchRequest'],
                filter: 'username eq "' + email + '"',
                count: 1,
                startIndex: 1
            })
            .withExpectStatus([200])
            .send()
            .then((searchResponse) => {
                if (searchResponse.body.totalResults > 0) {
                    const user = searchResponse.body.Resources[0];
                    console.log(`[AidAccountRestClient] search found [${email}] as ${user.id}`);
                    return {
                        id: user.id,
                        emails: user.emails,
                        displayName: user.displayName
                    };
                } else {
                    console.log(`[AidAccountRestClient] search did not find [${email}]`);
                    return undefined;
                }
            });
    }

    async createUser({email, displayName, password, validatedEmails}) {
        const path = '/Users';
        console.log(`[AidAccountRestClient] POST ${this.httpClient.config.baseUrl}${path} [${email}]`);
        const user = this.buildUser({email, displayName, password, validatedEmails});
        return await this.httpAuthClient
            .withMethodPost()
            .withPath(path)
            .withJsonBody(user)
            .withExpectStatus([201])
            .send()
            .then((response) => {
                const id = response.body.id;
                if (id) {
                    console.log(`[AidAccountRestClient] Created user [${email}] with id [${id}]`);
                    return response.body;
                } else {
                    throw new Error('[AidAccountRestClient] Unable to create user: ' + email);
                }
            });
    }

    async updateUser({id, email, displayName, password, validatedEmails}) {
        const path = `/Users/${id}`;
        console.log(`[AidAccountRestClient] PUT ${this.httpClient.config.baseUrl}${path}`);
        const user = this.buildUser({email, displayName, password, validatedEmails});
        return await this.httpAuthClient
            .withMethodPut()
            .withPath(path)
            .withJsonBody(user)
            .withExpectStatus([200])
            .send()
            .then(() => {
                console.log(`[AidAccountRestClient] Updating user [${email}] with id [${id}]`);
                return {id};
            });
    }

    async createOrUpdateUser({email, displayName, password, validatedEmails}) {
        return await this.searchUser({email})
            .then((user) => {
                if (user) {
                    return this.updateUser({id: user.id, email, displayName, password, validatedEmails});
                } else {
                    return this.createUser({email, displayName, password, validatedEmails});
                }
            });
    }

    async deleteUserIfExist({email}) {
        return await this.searchUser({email})
            .then((user) => {
                if (user) {
                    return this.deleteUser({id: user.id});
                } else {
                    console.log(`[AidAccountRestClient] User with email [${email}] does not exist`);
                    return undefined;
                }
            });
    }

    async deleteUser({id}) {
        const path = `/Users/${id}`;
        console.log(`[AidAccountRestClient] DELETE ${this.httpClient.config.baseUrl}${path}`);
        return await this.httpAuthMgmtClient
            .withMethodDelete()
            .withPath(path)
            .withExpectStatus([204])
            .withDiscardBodyResponseHandler()
            .send()
            .then((response) => {
                console.log(`[AidAccountRestClient] Deleted user [${id}], status code ${response.statusCode}`);
                return {id};
            });

    }

    async searchUserById({id}) {
        const path = `/Users/${id}`;
        console.log(`[AidAccountRestClient] GET ${this.httpClient.config.baseUrl}${path}`);
        return await this.httpAuthClient
            .withMethodGet()
            .withPath(path)
            .withExpectStatus([200, 404])
            .send()
            .then((response) => {
                if (response.statusCode === 404) {
                    return undefined;
                } else {
                    console.log(`[AidAccountRestClient] User with with id [${id}] exists `);
                    return response.body;
                }
            });
    }

    async createRandomUser() {
        const username = `${uuid()}@sink.sendgrid.net`;
        const password = crypto.randomBytes(15);
        const createdUser = {
            email: username,
            displayName: 'PDV user',
            password: password.toString('base64'),
            validatedEmails: [ username ]
        };
        return await this.createUser(createdUser).then((createdUserResponse) => {
            return Object.assign({}, createdUser, createdUserResponse);
        });
    }
}

module.exports = {AidAccountRestClient};
