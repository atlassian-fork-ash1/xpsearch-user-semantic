'use strict';

const config = require('../../configuration');
const httplease = require('httplease');

const baseUrl = config.management.baseUrl;

class AidManagementRestClient {
    constructor({sessionToken, sessionCookieName}) {
        this.httpClient = httplease.builder()
            .withBaseUrl(baseUrl)
            .withTimeout(5000)
            .withBufferJsonResponseHandler()
            .withHeaders({
                'Cookie': `${sessionCookieName}=${sessionToken}; avatar.csrf.token=c3bebd381c18`,
                'X-CSRF-TOKEN': 'c3bebd381c18',
                'Referer': baseUrl
            });
    }

    async getUser() {
        console.log(`[AMClient] GET ${baseUrl}/manage/rest/user`);
        return await this.httpClient
            .withPath('/manage/rest/user')
            .withMethodGet()
            .withExpectStatus([200])
            .send()
            .then((response) => {
                return response.body;
            });
    }

    async updateUser(user) {
        console.log(`[AMClient] PUT ${baseUrl}/manage/rest/user`);
        return await this.httpClient
            .withPath('/manage/rest/user')
            .withMethodPut()
            .withJsonBody(user)
            .withExpectStatus([200])
            .send()
            .then((response) => {
                return response.body;
            });
    }

    async getApiTokens() {
        console.log(`[AMClient] GET ${baseUrl}/manage/rest/api-tokens`);
        return await this.httpClient
            .withPath('/manage/rest/api-tokens')
            .withMethodGet()
            .withExpectStatus([200])
            .send();
    }

    async deleteApiToken({apiTokenId}) {
        console.log(`[AMClient] DELETE ${baseUrl}/manage/rest/api-tokens/${apiTokenId}`);
        return await this.httpClient
            .withPath(`/manage/rest/api-tokens/${apiTokenId}`)
            .withMethodDelete()
            .withDiscardBodyResponseHandler()
            .withExpectStatus([204])
            .send();
    }

    async deleteAllApiTokens() {
        const apiTokenIds = (await this.getApiTokens()).body.map(({id}) => id);
        return await Promise.all(apiTokenIds.map((apiTokenId) => this.deleteApiToken({apiTokenId})));
    }

    async generateApiToken() {
        console.log(`[AMClient] POST ${baseUrl}/manage/rest/api-tokens`);
        const apiToken = await this.httpClient
            .withPath('/manage/rest/api-tokens')
            .withMethodPost()
            .withJsonBody({label: 'default'})
            .withExpectStatus([200])
            .send()
            .then((response) => {
                return response.body.passwordValue;
            });
        console.log('[AMClient] generated token', apiToken);
        return apiToken;
    }
}

module.exports = {AidManagementRestClient};